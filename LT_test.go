package LT

import "testing"

func TestGreeting(t *testing.T) {
	result := greeting()
	expected := "Hello, World."
	if result != expected {
		t.Errorf("greeting() failed. expect:%v, actual: %v", expected, result)
	}
	t.Logf("result is %v", result)
}
